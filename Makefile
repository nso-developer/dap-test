# Include standard NID (NSO in Docker) package Makefile that defines all
# standard make targets
include nidpackage.mk
DEBUGPY=daptest

# Start extra containers or place things you want to run once, after startup of
# the containers, in testenv-start-extra.
testenv-start-extra:
	@true # NOOP


testenv-test:
	@echo "\n== Running tests"
	$(MAKE) testenv-runcmdJ CMD="request daptest python-test"
