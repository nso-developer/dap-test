# -*- mode: python; python-indent: 4 -*-
import ncs
from ncs.dp import Action

class TestAction(Action):
    @Action.action
    def cb_action(self, uinfo, name, kp, action_input, action_output):
        foo = 1
        bar = 2
        if foo is True:
            action_output.message = "foo is True\n"
        else:
            action_output.message = "foo is not True\n"

        action_output.message += "\nHello world from Python, hopefully supporting DAP!"


class Main(ncs.application.Application):
    def setup(self):
        self.log.info('Main RUNNING')

        self.register_action('daptest-python-actionpoint', TestAction)


    def teardown(self):
        self.log.info('Main FINISHED')
